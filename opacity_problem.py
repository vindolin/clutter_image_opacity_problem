from gi.repository import Clutter, GdkPixbuf, Cogl

Clutter.init([])
stage = Clutter.Stage()
stage.set_size(600, 300)

# old style
texture_actor = Clutter.Texture(filename='icon_big_a.png')
texture_actor.set_opacity(127)
stage.add_child(texture_actor)

# replacement because ClutterTexture is deprecated
pixbuf = GdkPixbuf.Pixbuf.new_from_file('icon_big_b.png')
pixel_format = Cogl.PixelFormat.RGBA_8888 if pixbuf.get_has_alpha() \
    else Cogl.PixelFormat.RGB_888

image = Clutter.Image()
image.set_data(
    pixbuf.get_pixels(),
    pixel_format,
    pixbuf.get_width(),
    pixbuf.get_height(),
    pixbuf.get_rowstride(),
)

image_actor = Clutter.Actor()
image_actor.set_content_scaling_filters(
    Clutter.ScalingFilter.TRILINEAR,
    Clutter.ScalingFilter.LINEAR
)
image_actor.set_content(image)
image_actor.set_size(pixbuf.get_width(), pixbuf.get_height())
image_actor.set_opacity(127)
image_actor.move_by(300, 0)
stage.add_child(image_actor)

stage.show()
Clutter.main()

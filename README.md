As ClutterTexture is now marked as deprecated, I followed the recommendation and replaced it with a ClutterActor that has it's content set to a pixbuf.

Everything works but when I change the actors opacity to 127, it darkens the background even where it's white.

A is the ClutterTexture with opacity=127.

B is the ClutterActor with the pixbuf and opacity=127.

![screenshot](http://i.imgur.com/QHJs7D0.png)
